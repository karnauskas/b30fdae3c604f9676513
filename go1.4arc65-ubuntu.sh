#!/bin/sh

# OPTIONAL FLAGS:
#
# -geoip true 
#       this will install maxmind geoip and auto update crontab file
#
# -cloudwatch true
#       this will install aws cloud watch metrics and send them to aws dashboard
#
# -imagemagick true
#       this will install imagemagick and magickwand
#

# process command line args
while echo $1 | grep -q ^-; do
    eval $( echo $1 | sed 's/^-//' )=$2
    shift
    shift
done

# Install Golang 1.4 and Arc65 + dependencies on fresh Amazon AWS EC2 Ubuntu 14.04 LTS

echo "install correct language pack"
cat <<EOF > /etc/default/locale
LANG=en_US.UTF-8
LANGUAGE=en_US
LC_CTYPE=en_US.UTF-8
LC_ALL=en_US.UTF-8
EOF
. /etc/default/locale

echo "set correct timezone"
dpkg-reconfigure tzdata

echo "install basic requirements"
apt-get update && upgrade
apt-get install build-essential mercurial git libssl-dev pkg-config tree

echo "download Go and install it, as well as create GOPATH directory"
cd ~

wget https://storage.googleapis.com/golang/go1.4.1.linux-amd64.tar.gz
tar -xf go1.4.1.linux-amd64.tar.gz && rm go1.4.1.linux-amd64.tar.gz
mv go /usr/local && mkdir -p /usr/local/gopath

echo "set enviornment variables required for Go"
export GOROOT=/usr/local/go
export GOPATH=/usr/local/gopath
cat <<EOF >> ~/.bashrc
export GOROOT=/usr/local/go
export GOPATH=/usr/local/gopath
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
export GORACE=log_path=$GOPATH/racereport
export w=$GOPATH/src/github.com
EOF
. ~/.bashrc

# install Go tools
echo "installing go tool ... golint"
go get github.com/golang/lint/golint
echo "installing go tool ... errcheck"
go get github.com/kisielk/errcheck
echo "installing go tool ... benchcmp"
go get golang.org/x/tools/cmd/benchcmp
echo "installing go tool ... impl"
go get github.com/josharian/impl
echo "installing go tool ... goimports"
go get golang.org/x/tools/cmd/goimports
echo "installing go tool ... goreturns"
go get sourcegraph.com/sqs/goreturns
echo "installing go tool ... godef"
go get code.google.com/p/rog-go/exp/cmd/godef
echo "installing go tool ... gocode"
go get github.com/nsf/gocode
echo "installing go tool ... oracle"
go get golang.org/x/tools/cmd/oracle
echo "installing go tool ... gorename"
go get golang.org/x/tools/cmd/gorename
echo "installing go tool ... godepgraph"
go get github.com/kisielk/godepgraph

echo "install arc65 external dependencies"
apt-get install zlib1g-dev nodejs npm
npm install less -g
npm install clean-css -g
npm install uglify-js -g
npm install html-minifier -g
npm install autoprefixer -g
npm install bower-installer -g
ln -s /usr/bin/nodejs /usr/bin/node

echo "install wrk benchmark tool (apache bench (ab) alternative)"
cd ~
git clone https://github.com/wg/wrk.git
cd wrk
make
cp wrk /usr/local/bin
cd ~
rm -rf wrk

echo "tuning ubuntu for high concurrent connections"

cat <<EOF >> /etc/security/limits.conf
root       soft    nofile  40000
root       hard    nofile  40000
*          soft    nofile  40000
*          hard    nofile  40000
EOF

cat <<EOF >> /etc/pam.d/su
session    required   pam_limits.so
EOF

echo "-cloudwatch flag will enable pushing ec2 metrics to aws dashboard"
if $cloudwatch == "true"; then
cd ~

echo "install Amazon AWS command line client"
apt-get install awscli

echo "set enviornment variables for getting ec2 instance id and region"
cat <<EOF >> ~/.bashrc
export INSTANCE_ID="`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id`"
export EC2_REGION="`wget -q -O - http://169.254.169.254/latest/meta-data/placement/availability-zone | sed -e 's:\([0-9][0-9]*\)[a-z]*\$:\\1:'`"
EOF
. ~/.bashrc

echo "install custom cloudwatch metrics"
git clone https://github.com/mapbox/cwput.git
cd cwput
./install.bash

echo "register with crontab to ensure metrics are sent every minute"
cat <<EOF > /etc/cron.d/cloudwatch
* * * * * root . $HOME/.bashrc; /usr/bin/cwput.bash
EOF
chmod 755 /etc/cron.d/cloudwatch
fi

if $imagemagick == "true"; then
echo "install image magick and magickwand"
echo "useful for advanced image manipulation in go using 3rd party golang packages with C bindings using cgo"
echo "e.g. https://github.com/gographics/imagick"
apt-get install imagemagick libgif-dev libmagickwand-dev
fi

if $geoip == "true"; then
echo "maxmind geoip mmdblookup c library install"
echo "useful for converting IP to city/country data structure in go using 3rd party golang packages with C bindings using cgo"
echo "e.g. https://github.com/oschwald/geoip2-golang"
cd ~
git clone --recursive https://github.com/maxmind/libmaxminddb
cd libmaxminddb
apt-get install dh-autoreconf
./bootstrap
./configure
make check
make install
ldconfig
cd ~
rm -rf ./tmp/
rm -rf ./libmaxminddb/

echo "install maxmind geoip update command line tool"
add-apt-repository ppa:maxmind/ppa
apt-get update
apt-get install geoipupdate

cat <<EOF >> /etc/GeoIP.conf
# GeoIP.conf file - used by geoipupdate program
# to update databases from http://www.maxmind.com
# UserId, LicenseKey, ProductIds from einthusan@wojka.com account
UserId 94685
LicenseKey XXXXXXXXXX
ProductIds 106 133 GeoIP2-City GeoIP2-Country
EOF

echo "update GeoIP for the first time"
geoipupdate

echo "register with crontab to ensure auto update"
cat <<EOF > /etc/cron.d/geoip
MAILTO=einthusan@wojka.com
56 8 * * 5 root /usr/bin/geoipupdate
EOF
chmod 755 /etc/cron.d/geoip
fi

echo "install done."